---
layout: markdown_page
title: Stewardship
---

- TOC
{:toc}

## Our stewardship of GitLab CE

### Business model

GitLab Inc. is a for profit company that balances the need to improve
GitLab Community Edition (CE) with the need to add features to GitLab
Enterprise Edition (EE) exclusively in order to generate income.
We have an [open core](https://en.wikipedia.org/wiki/Open_core) business model and generate almost all our revenue with [subscriptions to use Enterprise Edition](https://about.gitlab.com/pricing/).
We recognize that we need to balance the need to generate income and with the needs of the open source project.

### Promises

We promise that:

1. We won't remove features from CE in order to make the same feature exclusive in EE (features might be removed from CE due to changes in the application)
1. We won't introduce features into CE with a delay, if a feature is planned to land in both it will be released simultaneously in both
1. We will always release all tests that we have for a feature that is in CE
1. CE will have all the features that are essential to running a large 'forge' with public and private repositories
1. CE will not contain any artificial limits (repositories, users, size, performance, etc.)
1. All major features in [our scope](https://about.gitlab.com/direction/#scope) will be available in GitLab CE too
1. The majority of new features made by GitLab Inc. will be available in both CE and EE
1. CE will be available for download without leaving an email address

### What features are EE only

If the wider community contributes a new feature they get to choose if it goes into CE or EE by
sending the merge request to the repository they prefer (most go to CE). If the wider community
contributes a feature already in EE to CE we use the process linked in
[Contributing an EE only feature to CE](#contributing-an-ee-only-feature-to-ce).
When GitLab Inc. makes a new feature we ask ourselves,
is this feature **more relevant for larger organizations**?
If the answer is yes the feature is likely to be exclusive to EE.

There aren't any features that are only useful to larger organizations,
so for every EE feature there will be smaller organizations that might need it.
We're not saying that there aren't any small organizations that need the EE feature,
just that we think that larger organizations are more likely to need it.

It is hard to get CE vs. EE right, and if we put something in EE that should be in CE we won't hesitate to [open-source](https://about.gitlab.com/2016/12/24/were-bringing-gitlab-pages-to-community-edition/) [it](https://news.ycombinator.com/item?id=10931347).

We always make sure that CE can do all major features in [our scope](https://about.gitlab.com/direction/#scope) and there are companies using CE with more than 10,000 users.

If people ask us why a certain feature is EE only we might reply with a [link to this section of the handbook](https://about.gitlab.com/stewardship#what-features-are-ee-only). We do not mean to imply you don't need the feature. It implies we think the feature will be more relevant for larger organizations. Feel free to make the argument for the opposite, we're listening.

### What features are EE Premium

[See the definition of EE Premium features in our product handbook](/handbook/product/#enterprise-edition-tiers).

### Why release simultaneously in both

Sometimes people suggest having features in EE for a limited time.
An example of a limited time release strategy is the [Business Source License](https://mariadb.com/bsl) that keeps features propietary for 3 years.
At GitLab we want to give everyone access to most of the features (and all the essential ones) at the date they are announced.
We want to give people the option to both run and contribute to an open source edition that is maintained and that includes the most recent security fixes.
From time to time we do open source a feature that used to be EE only.
We do this when we realize that we've made a mistake applying our criteria, for example
when we learned that a branded homepage was an [essential feature](https://news.ycombinator.com/item?id=10931347) or
when we [brought GitLab Pages to the Community Edition](https://about.gitlab.com/2016/12/24/were-bringing-gitlab-pages-to-community-edition/).

### How the GitLab Inc. benefits CE

Apart from making new features GitLab Inc. does a lot of work that benefits both CE and EE:

1. [Responsible disclosure](https://about.gitlab.com/disclosure/) process and security fixes
1. Release management including a monthly release of both CE and EE
1. Packaging GitLab in our [Omnibus packages](https://gitlab.com/gitlab-org/omnibus-gitlab)
1. Running a [packages server](https://packages.gitlab.com/gitlab/)
1. Dependency upgrades (Rails, gems, etc.)
1. Performance improvements

### Contributing an EE only feature to CE

When someone contributes a feature to CE that is already in EE we have a hard decision to make.
We hope that people focus on contributing features that are neither in CE nor EE.
This way both editions benefit from a new feature and GitLab Inc. doesn't have to make a difficult decision.
The features we plan to build for EE are shared on our [direction page](https://about.gitlab.com/direction/) and we welcome people to contribute features to CE that are planned for future EE releases. If you pick one from the upcoming release please ask in the issue if someone is already working on it.
When someone does contribute a feature to CE that is already in EE we weigh a couple of factors in that decision:

1. What is the quality of the submitted code?
1. Is it a complete replacement of the EE functionality?
1. Does it meet the criteria of the [definition of done](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#definition-of-done)?
1. Is it more relevant for organizations that have more than 100 potential users?
1. Is the person or organization submitting this using GitLab with more or less than 100 potential users?
1. Did the person or organization submitting this contribute to GitLab before?
1. Is it something that many of our existing customers chose GitLab Enterprise Edition for?
1. Is it relevant for running a large open source forge?
1. Is it an original work or based on the EE code?
1. Is there an actively maintained fork that incorporates this?
1. How many organizations are using this code in production?
1. How frequently has this functionality been requested for CE and by whom?

We'll weight all factors and you can judge our stewardship of CE based on the outcome. So far (July 22, 2016) we had only two cases: One had low code quality and the other one copied the EE code down to the last space. If you find these or other examples please link them here so people can get an idea of the outcome.

In case we're not sure, we'll consult with the [core team](https://about.gitlab.com/core-team/) to reach a conclusion.
